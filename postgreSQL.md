## PostgeSQL

### Instalación
* **Install using Docker** (Recomendada)

1. [How to install docker](docker.md)

2. Decargar el docker de postgreSQL
```bash
docker pull postgres
```

3. Iniciar una instancia de postgreSQL 
```bash
docker run --name docker-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

4. Ver instancias que estan corriendo
```bash
docker ps x
```

5. Entrar al Aministrador de BD
```bash
docker exec -it --user postgres docker-postgres  bash
```
```bash
psql
```

fuente: [hub.docker.com/_/postgres/](https://hub.docker.com/_/postgres/)


* **Install in Debian / Ubuntu**
```bash
sudo apt update
sudo apt update && sudo apt install postgresql postgresql-contrib
```
fuente: [how-to-install-postgresql-on-ubuntu-20-04-quickstart](https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart)



* **Install in Fedora**
1. Actualizar
```bash
sudo dnf install -y vim bash-completion wget
sudo dnf update -y
sudo reboot
```

2. Agregar los repositorios de PostgreSQL  ( Fedora 35 )
```bash
sudo dnf install https://download.postgresql.org/pub/repos/yum/reporpms/F-35-x86_64/pgdg-fedora-repo-latest.noarch.rpm
```

3. Instalar
```bash
sudo dnf install postgresql12-server postgresql12
```

4. Iniciar base de datos
```bash
sudo /usr/pgsql-12/bin/postgresql-12-setup initdb
```

5. Habilitar el servicio en el boot
```bash
sudo systemctl enable --now postgresql-12
```

6. Revisar el estado del servicio
```bash
systemctl status postgresql-12 
```

fuente: [how-to-install-postgresql-12-on-fedora](https://computingforgeeks.com/how-to-install-postgresql-12-on-fedora/)

------