 # Curso de Base de Datos

 ## ¿ Como descargar el contenido ?
Hay dos formas
1.  Desde la web del proyecto en gitlab 
![Botón de Descarga](/resources/imgs/download.png "Descarga")

2. Por consola
```bash
git clone https://gitlab.com/l30bravo/db_udp.git
```
## Programa del Curso
* ![Programa del Curso de Base de Datos 2022](/resources/pdf/programa_curso_base_de_datos.pdf "Programa del Curso")
* ![Conograma del Curso](/resources/Template_planificacion_evaluaciones_2022.xlsx "Cronograma del Curso")

## Clases
* ![bdd_1_ Introducción ODP](/resources/clases/bdd_1_%20Introducci%C3%B3n.odp "bdd_1_ Introducción.odp")
* ![bdd_1_ Introducción PDF](/resources/clases/bdd_1_%20Introducci%C3%B3n.pdf "bdd_1_ Introducción.pdf")

## Apuntes
* [PostgreSQL](postgreSQL.md)

## Bibliografia
* ![Introducción a los sistemas de Base de Datos](/resources/pdf/Introduccion%20a%20los%20Sistemas%20de%20Bases%20de%20Datos%20-%207ma%20Edicion%20-%20C.%20J.%20Date.pdf "Introducción a los sistemas de Base de Datos")